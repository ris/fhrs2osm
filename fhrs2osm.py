#!/usr/bin/env python
"""
	fhrs2osm for creating .osm files from fhrs xml files
	
	These files are intended to be loaded into an editor (josm) on a separate layer and
	manually merged with the existing data. They are simply for convenience of editing.
	
	If I find anyone going and uploading the resulting files to OSM I will be angry.
		-- ris
"""
__author__ = "Robert Scott"
__license__ = "GPLv3"


from xml.etree.cElementTree import parse , ElementTree , Element , SubElement
from re import compile

_addr_re = compile ( r"^(.*(\b\S*\d\S*\b)\s*)(\D+)$" )
_spaced_dash_re = compile ( r"\s+-\s+" )

def convert_fhrs ( fhrs_et ):
	node_id_counter = -1
	
	osm_root = Element ( "osm" , version = "0.6" , generator = "fhrs2osm" )
	osm_et = ElementTree ( osm_root )
	
	for ed in fhrs_et.iterfind ( "EstablishmentCollection/EstablishmentDetail" ):
		geocode_lat = ed.findtext ( "Geocode/Latitude" )
		if geocode_lat is None:
			continue
		geocode_lon = ed.findtext ( "Geocode/Longitude" )
		if geocode_lon is None:
			continue
		
		node = SubElement ( osm_root , "node" , id = unicode ( node_id_counter ) , visible = "true" , lat = geocode_lat , lon = geocode_lon )
		node_id_counter -= 1
		
		business_name = ed.findtext ( "BusinessName" )
		if business_name is not None:
			SubElement ( node , "tag" , k = "checkfirst:suggested:name" , v = business_name )
		
		postcode = ed.findtext ( "PostCode" )
		if postcode is not None:
			SubElement ( node , "tag" , k = "checkfirst:suggested:addr:postcode" , v = postcode )
		
		business_type = ed.findtext ( "BusinessType" )
		if business_type is not None:
			SubElement ( node , "tag" , k = "dontimport:fhrs:businesstype" , v = business_type )
		
		addrline1 = ed.findtext ( "AddressLine1" )
		if addrline1 is not None:
			SubElement ( node , "tag" , k = "dontimport:fhrs:addrline1" , v = addrline1 )
		
		addrline2 = ed.findtext ( "AddressLine2" )
		if addrline2 is not None:
			SubElement ( node , "tag" , k = "dontimport:fhrs:addrline2" , v = addrline2 )
		
		addrline3 = ed.findtext ( "AddressLine3" )
		if addrline3 is not None:
			SubElement ( node , "tag" , k = "dontimport:fhrs:addrline3" , v = addrline3 )
		
		addrline4 = ed.findtext ( "AddressLine4" )
		if addrline4 is not None:
			SubElement ( node , "tag" , k = "dontimport:fhrs:addrline4" , v = addrline4 )
		
		for addrline in ( addrline1 , addrline2 , addrline3 , addrline4 ):
			if addrline is not None:
				match = _addr_re.match ( addrline )
				if match is not None:
					SubElement ( node , "tag" , k = "checkfirst:suggested:addr:housenumber" , v = _spaced_dash_re.sub ( "-" , match.group ( 1 ).strip () ) )
					SubElement ( node , "tag" , k = "checkfirst:suggested:addr:street" , v = match.group ( 3 ).strip () )
					break
		
		rating_value = ed.findtext ( "RatingValue" )
		if rating_value is not None:
			SubElement ( node , "tag" , k = "fhrs:rating" , v = rating_value )
		
		la_name = ed.findtext ( "LocalAuthorityName" )
		if la_name is not None:
			SubElement ( node , "tag" , k = "fhrs:authority" , v = la_name )
		
		fhrs_id = ed.findtext ( "FHRSID" )
		if fhrs_id is not None:
			SubElement ( node , "tag" , k = "fhrs:id" , v = fhrs_id )
		
		rating_date = ed.findtext ( "RatingDate" )
		if rating_date is not None:
			SubElement ( node , "tag" , k = "fhrs:inspectiondate" , v = rating_date )
	
	return osm_et

if __name__ == "__main__":
	import argparse
	from sys import stdin , stdout , stderr
	ap = argparse.ArgumentParser ()
	ap.add_argument ( "infile" , nargs = "?" , type = argparse.FileType ( "r" ) , default = stdin , help = "input FHRS XML file (else stdin)" )
	ap.add_argument ( "outfile" , nargs = "?" , type = argparse.FileType ( "w" ) , default = stdout , help = "output OSM XML file (else stdout)" )
	args = ap.parse_args ()
	
	osm_et = convert_fhrs ( parse ( args.infile ) )
	osm_et.write ( args.outfile , encoding = "utf8" )
	
	print >> stderr , "Done. Now promise me you won't just go and upload the result."
